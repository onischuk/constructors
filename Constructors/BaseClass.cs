namespace Constructors;

public class BaseClass
{
    private readonly Guid? _id;
    
    public BaseClass()
    {
        _id = Guid.NewGuid();
        DisplayValue();
    }

    protected virtual void DisplayValue()
    {
        Console.WriteLine($"Base class value: {_id.Value.ToString()}");
    }
}