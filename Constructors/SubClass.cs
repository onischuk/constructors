namespace Constructors;

public class SubClass: BaseClass
{
    private readonly Guid? _id;

    public SubClass()
    {
        _id = Guid.NewGuid();
    }

    protected override void DisplayValue()
    {
        Console.WriteLine($"Sub class id: {_id.Value.ToString()}");
    }
}